package com.example.zooactivity

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zooactivity.databinding.FragmentGettingStartedBinding
import com.example.zooactivity.databinding.FragmentHelloBinding

class GettingStarted : Fragment() {
    private var _binding: FragmentGettingStartedBinding? = null
    private val binding: FragmentGettingStartedBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingStartedBinding.inflate(inflater, container,false).also {
        _binding = it
    }.root
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }
private fun initListeners() = with(binding){
    back.setOnClickListener {
        val action = com.example.zooactivity.GettingStartedDirections.actionGettingStartedToHelloFragment()
        findNavController().navigate(action)
    }
    next2.setOnClickListener {
        val action2 = com.example.zooactivity.GettingStartedDirections.actionGettingStartedToEnjoyVisitFragment2()
        findNavController().navigate(action2)
    }
}


//    initListeners()
//}
//private fun initListeners() = with(binding){
//    next.setOnClickListener {
//        val action = com.example.zooactivity.HelloFragmentDirections.actionHelloFragmentToGettingStarted()
//        findNavController().navigate(action)
//        //val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment()
//        //findNavController().navigate(action)
//    }
//}











    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
    }

}