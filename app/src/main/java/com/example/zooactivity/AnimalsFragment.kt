package com.example.zooactivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.zooactivity.databinding.FragmentAnimalsBinding
import com.example.zooactivity.databinding.FragmentHelloBinding
import kotlin.math.log

class AnimalsFragment : Fragment() {
    private var _binding: FragmentAnimalsBinding? = null
    private val binding: FragmentAnimalsBinding get()= _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentAnimalsBinding.inflate(inflater, container,false).also {
        _binding = it

    }.root

   // Log.d("com.example.zooactivity", "onStop")
    //Toast.makeText(applicationContext, "onStop", Toast.LENGTH_SHORT).show()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
       Toast.makeText(activity,"Welcome to zoo Safari", Toast.LENGTH_LONG).show()

   }
    private fun initListeners() = with(binding) {

        tiger.setOnClickListener {
            val action =
                AnimalsFragmentDirections.actionAnimalsFragmentToDetailFragment("Tiger",
                        "-> Tigers are the largest cat species in the world reaching up to 3.3 meters in length and weighing up to 670 pounds!\n" +
                        "-> Tigers are easily recognizable with their dark vertical stripes and reddish/orange fur.\n" +
                        "-> The Bengal tiger is the most common tiger.\n" +
                        "-> Tigers live between 20-26 years in the wild.")

            findNavController().navigate(action)
        }
        crocodile.setOnClickListener {
            val action =
                AnimalsFragmentDirections.actionAnimalsFragmentToDetailFragment("Crocodile","->Crocodiles are reptiles.\n" +
                        "->The physical characteristics of crocodiles make them good predators.\n" +
                        "->Crocodiles are fast over short distances.\n" +
                        "->Crocodiles have sharp teeth.\n" +
                        "->Crocodiles have the strongest bite of any animal in the world.")

            findNavController().navigate(action)
        }
        monkey.setOnClickListener {
            val action =
                AnimalsFragmentDirections.actionAnimalsFragmentToDetailFragment("Monkey","\n" +

                        "->Not All Primates Are Monkeys. ...\n" +
                        "->Many Monkeys Are at Risk. ...\n" +
                        "->They Use Grooming To Strengthen Relationships. ...\n" +
                        "->Only New World Monkeys Have Prehensile Tails. ...\n" +
                        "->There's Only One Species of Wild Monkey in Europe. ...\n" +
                        "->Pygmy Marmosets Are the World's Smallest Monkeys. ...\n" +
                        "->Mandrills Are the World's Largest Monkeys.")

            findNavController().navigate(action)
        }
        giraffe.setOnClickListener {
            val action =
                AnimalsFragmentDirections.actionAnimalsFragmentToDetailFragment("Giraffe",
                    "->Giraffes are the tallest mammals on Earth. Their legs" +
                            " alone are taller than many humans—about 6 feet.[1]\n" +
                        "->They can run as fast as 35 miles an hour over short distances," +
                            " or cruise at 10 mph over longer distances.\n" +
                        "->A giraffe's neck is too short to reach the ground. As a result" +
                            ", it has to awkwardly spread its front legs or kneel to reach" +
                            " the ground for a drink of water.[3]\n" +
                        "->Giraffes only need to drink once every few days. Most of their water" +
                            " comes from all the plants they eat.\n" +
                        "->Giraffes spend most of their lives standing up; they even sleep " +
                            "and give birth standing up.\n" +
                        "->The giraffe calf can stand up and walk after about an " +
                            "hour and within a week, it starts to sample vegetation.")

            findNavController().navigate(action)
        }


    }
}