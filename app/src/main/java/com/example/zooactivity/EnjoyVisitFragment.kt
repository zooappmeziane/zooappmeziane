package com.example.zooactivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.zooactivity.databinding.FragmentEnjoyVisitBinding

class EnjoyVisitFragment : Fragment() {
    private var _binding: FragmentEnjoyVisitBinding? = null
    private val binding: FragmentEnjoyVisitBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentEnjoyVisitBinding.inflate(inflater, container,false).also {
        _binding = it
    }.root
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }
    private fun initListeners() = with(binding){
        back3.setOnClickListener {
            val action = EnjoyVisitFragmentDirections.actionEnjoyVisitFragmentToGettingStarted()
            findNavController().navigate(action)
        }
        finish.setOnClickListener{
            val action = EnjoyVisitFragmentDirections.actionEnjoyVisitFragmentToAnimalzoo()
            findNavController().navigate(action)
        }

    }

}